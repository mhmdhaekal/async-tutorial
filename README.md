### Muhamamd Haekal Kalipaksi

### 2206817490

# Tutorial 1

### 1.2 Understanding how it works

```rs
fn main() {
    let (executor, spawner) = new_executor_and_spawner();

    // Spawn a task to print before and after waiting on a timer.
    spawner.spawn(async {
        println!("Haekal's Computer: howdy!");
        TimerFuture::new(Duration::new(2, 0)).await;
        println!("Haekal's Computer: done!");
    });

    println!("Haekal's Computer: hey hey");

    // Drop the spawner so that our executor knows it is finished and won't
    // receive more incoming tasks to run.
    drop(spawner);

    // Run the executor until the task queue is empty.
    // This will print "howdy!", pause, and then print "done!".
    executor.run();
}
```

Based on the code above and the implementation of futures, spawner.spawn actually send task into tasks queue

```rs
fn spawn(&self, future: impl Future<Output = ()> + 'static + Send) {
    let future = future.boxed();
    let task = Arc::new(Task {
        future: Mutex::new(Some(future)),
        task_sender: self.task_sender.clone(),
    });
    self.task_sender.send(task).expect("too many tasks queued");
}
```

when i add `println!("Haekal's Computer: hey hey");` it actualy run first since the tasks will execute all in `executor.run()`. Code output:

```shell
cargo run -p async_tutorial
    Finished dev [unoptimized + debuginfo] target(s) in 0.02s
     Running `target/debug/async_tutorial`
Haekal's Computer: hey hey
Haekal's Computer: howdy!
Haekal's Computer: done!
```

As we can see, `hey hey` will run first before the task howdy and wait done

### 1.3 Multiple spawn and removing drop

#### Multiple spawn

![async-1](images/async-1.png?raw=true)

As expected when adding multiple spawn, it will push multple task into tasks queue which resulting when the parts of code `executor.run()` the all task will be run concurrently, the output print at the same time.

### Removing drop

![async-2](images/async-2.png?raw=true)

Based on screenshot above we can when dropping the spawner, the code will not exit. Executor will still waiting the new task and spawner still expecting sending and receiving task.

# Tutorial 2

### Original code of broadcast chat

![server](images/server.png?raw=true)

![client-1](images/client-1.png?raw=true)

![client-2](images/client-2.png?raw=true)

![client-3](images/client-3.png?raw=true)

Based on all screenshoot above, when client sending a messages server will received it and send back into all client. It happens since client will wait a messages from server and print it. based on this part of code:

```rs
loop {
    tokio::select! {
        incoming = ws_stream.next() => {
            match incoming {
                Some(Ok(msg)) => {
                    if let Some(text) = msg.as_text() {
                        println!("From server: {}", text);
                    }
                },
                Some(Err(err)) => return Err(err.into()),
                None => return Ok(()),
            }
        }
    }
```

and server will receive a messages and send back into client based on this part of code:

```rs
loop {
    tokio::select! {
        incoming = ws_stream.next() => {
            match incoming {
                Some(Ok(msg)) => {
                    if let Some(text) = msg.as_text() {
                        println!("From client {addr:?} {text:?}");
                        bcast_tx.send(text.into())?;
                    }
                }
                Some(Err(err)) => return Err(err.into()),
                None => return Ok(()),
            }
        }
        msg = bcast_rx.recv() => {
            ws_stream.send(Message::text(msg?)).await?;
        }
    }
}
```

All operation based on websocket which make possible the all operation run at the same time for all client.

### Modifying the websocket port

In order to modifying the websocket port, i have to change in both client and server, here how i can change the port

### client

```rs
let (mut ws_stream, _) = ClientBuilder::from_uri(Uri::from_static("ws://127.0.0.1:8080"))
    .connect()
    .await?;
```

### server

```rs
let listener = TcpListener::bind("127.0.0.1:8080").await?;
println!("listening on port 8080");
```

In server code i change the TcpListener to bind port 8080 and the code the wrap the tcplistener into websocket

```rs
tokio::spawn(async move {
    let ws_stream = ServerBuilder::new().accept(socket).await?;

    handle_connection(addr, ws_stream, bcast_tx).await
});
```

### 2.3 Small Changes

![small-1](images/small-1.png?raw=true)

![small-2](images/small-2.png?raw=true)

![small-3](images/small-3.png?raw=true)

This is what i change

```rs
if let Some(text) = msg.as_text() {
    let message: String = format!("{addr:?} :  {text:?}");
    println!("From client {addr:?}: {text:?}");
    bcast_tx.send(message)?;
}
```

```rs
Some(Ok(msg)) => {
    if let Some(text) = msg.as_text() {
        println!("Haekal's Computer - From server: {}", text);
    }
},
```
