use futures_util::sink::SinkExt;
use futures_util::stream::StreamExt;
use serde::{Deserialize, Serialize};
use std::{collections::HashMap, error::Error, net::SocketAddr, sync::Arc};
use tokio::{
    net::{TcpListener, TcpStream},
    sync::{mpsc, Mutex},
};
use tokio_tungstenite::{
    accept_async, tungstenite::protocol::Message as WebSocketMessage, WebSocketStream,
};

#[derive(Debug, Clone, Serialize, Deserialize)]
struct Message {
    message_type: String,
    data: Option<String>,
    data_array: Option<Vec<String>>,
}

#[derive(Debug, Clone)]
struct User {
    nick: String,
    is_alive: bool,
    sender: mpsc::Sender<WebSocketMessage>,
}

async fn broadcast(
    users: &Arc<Mutex<HashMap<SocketAddr, User>>>,
    message: String,
) -> Result<(), Box<dyn std::error::Error + Send + Sync>> {
    let message = WebSocketMessage::Text(message);
    let users_to_send;

    {
        let users_guard = users.lock().await;
        users_to_send = users_guard
            .iter()
            .filter_map(|(&addr, user)| {
                if user.is_alive {
                    Some((addr, user.sender.clone()))
                } else {
                    None
                }
            })
            .collect::<Vec<(SocketAddr, mpsc::Sender<WebSocketMessage>)>>();
    }

    for (_, sender) in users_to_send {
        sender.send(message.clone()).await?;
    }

    Ok(())
}

async fn broadcast_user_list(
    users: &Arc<Mutex<HashMap<SocketAddr, User>>>,
) -> Result<(), Box<dyn std::error::Error + Send + Sync>> {
    let users_guard = users.lock().await;
    let user_list: Vec<String> = users_guard
        .iter()
        .map(|(_, user)| user.nick.clone())
        .collect();
    drop(users_guard); // Manually drop the guard to unlock before broadcasting

    let message = WebSocketMessage::Text(serde_json::to_string(&Message {
        message_type: "users".to_string(),
        data: None,
        data_array: Some(user_list),
    })?);
    broadcast(users, message.to_string()).await
}

async fn handle_connection(
    addr: SocketAddr,
    ws: WebSocketStream<TcpStream>,
    users: Arc<Mutex<HashMap<SocketAddr, User>>>,
) -> Result<(), Box<dyn Error + Send + Sync>> {
    println!("New connection from: {addr:?}");
    let (tx, mut rx) = mpsc::channel::<WebSocketMessage>(32);

    let user = User {
        nick: String::new(),
        is_alive: true,
        sender: tx,
    };
    {
        let mut users_guard = users.lock().await;
        users_guard.insert(addr, user);
    }

    let (mut write, mut read) = ws.split();

    tokio::spawn(async move {
        while let Some(message) = rx.recv().await {
            if let Err(e) = write.send(message).await {
                eprintln!("Error sending message over websocket: {e}");
                break;
            }
        }
    });

    while let Some(message_result) = read.next().await {
        match message_result {
            Ok(WebSocketMessage::Text(text)) => {
                let parsed_message: Message = serde_json::from_str(&text)?;
                if let Some(user) = users.lock().await.get_mut(&addr) {
                    match parsed_message.message_type.as_ref() {
                        "register" => {
                            if let Some(data) = parsed_message.data {
                                println!("{}", data);
                                user.nick = data.clone();
                                broadcast_user_list(&users).await?;
                            }
                        }
                        "message" => {
                            if let Some(data) = parsed_message.data {
                                broadcast(&users, data.to_string()).await?;
                            }
                        }
                        _ => {}
                    }
                }
            }
            Ok(_) => {}
            Err(e) => {
                println!("Error processing message: {e}");
                break;
            }
        }
    }

    let mut users_guard = users.lock().await;
    if let Some(user) = users_guard.get_mut(&addr) {
        user.is_alive = false;
    }
    users_guard.remove(&addr);
    Ok(())
}

#[tokio::main]
async fn main() {
    let addr = "127.0.0.1:8080";
    let listener = TcpListener::bind(addr).await.unwrap();
    println!("Listening on: {addr}");

    let users = Arc::new(Mutex::new(HashMap::<SocketAddr, User>::new()));

    while let Ok((stream, addr)) = listener.accept().await {
        let users_clone = users.clone();
        let ws_stream = accept_async(stream)
            .await
            .expect("Failed to accept websocket");
        tokio::spawn(async move {
            if let Err(e) = handle_connection(addr, ws_stream, users_clone).await {
                eprintln!("Error handling connection: {:?}", e);
            }
        });
    }
}
